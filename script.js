function pricing() {
    const checkbox = document.getElementById('checkbox');
    const amount1 = document.getElementById('amount1');
    const amount2 = document.getElementById('amount2');
    const amount3 = document.getElementById('amount3');

    if (checkbox.checked === true) {
      amount1.innerHTML = '$19.99';
      amount2.innerHTML = '$24.99';
      amount3.innerHTML = '$39.99'
    } else {
      amount1.innerHTML = '$199.99';
      amount2.innerHTML = '$249.99';
      amount3.innerHTML = '$399.99';
    }
  }